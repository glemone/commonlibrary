package com.dynamodb.common.library.commonlibrary;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

@Component
public interface ICommonLibrary {
	
	PutItemOutcome insertDataIntoDynamoDB(Item entity, String table);
	
	Item retrieveItemWithPrimaryKey(String table,String primaryKey,Object index);
	
	UpdateItemOutcome updateRecords(String table, String primaryKey, Object index,String expression,Map<String, String> expressionAttributeNames, 
			Map<String, Object> expressionAttributeValues);
	
	DeleteItemOutcome  deleteRecordsUsingWitId(String table,String primaryKey, Object index);
	
	DeleteItemOutcome  deleteRecordsUsingWithCondition(String table, String primaryKey, Object index ,String expression, Map<String, String> expressionAttributeNames, 
			Map<String, Object> expressionAttributeValues);
	
	ItemCollection<ScanOutcome> retrieveItems(String table);
	
	ItemCollection<QueryOutcome> getDataWithQuery(String table, QuerySpec query);
	
	ScanResult getDataWithScan(String table, ScanRequest query);
	
	UpdateItemOutcome updateRecordsWithItems(String table, UpdateItemSpec updateItemSpec);

	

}
