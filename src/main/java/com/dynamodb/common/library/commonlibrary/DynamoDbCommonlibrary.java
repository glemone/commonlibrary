package com.dynamodb.common.library.commonlibrary;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

@Component
public class DynamoDbCommonlibrary implements ICommonLibrary {

	private DynamoDB dynamoDb;	
	//private Regions REGION = Regions.US_EAST_2;

	private void initDynamoDbClient() {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		dynamoDb = new DynamoDB(client);
	}

	@Override
	public PutItemOutcome insertDataIntoDynamoDB(Item entity, String table) throws ConditionalCheckFailedException {
		initDynamoDbClient();					
		return dynamoDb.getTable(table).putItem( new PutItemSpec().withItem(entity));
	}

	@Override
	public Item retrieveItemWithPrimaryKey(String table, String primaryKey, Object index) {
		initDynamoDbClient();
		return dynamoDb.getTable(table).getItem(primaryKey,index);
	}
	
	@Override
	public ItemCollection<ScanOutcome> retrieveItems(String table) {
		initDynamoDbClient();
		System.out.println("Start Scanning table:" + table);
		ItemCollection<ScanOutcome> items = dynamoDb.getTable(table).scan();
		return items;
	}

	@Override
	public UpdateItemOutcome updateRecords(String table, String primaryKey, Object index,String expression, Map<String, String> expressionAttributeNames,
			Map<String, Object> expressionAttributeValues) {
		initDynamoDbClient();		
		return dynamoDb.getTable(table).updateItem(primaryKey,index,expression,expressionAttributeNames,expressionAttributeValues);
	}

	@Override
	public DeleteItemOutcome deleteRecordsUsingWitId(String table, String primaryKey, Object index) {
		initDynamoDbClient();
		return dynamoDb.getTable(table).deleteItem(primaryKey, index);
	}

	@Override
	public DeleteItemOutcome deleteRecordsUsingWithCondition(String table,  String primaryKey, Object index,String expression,
			Map<String, String> expressionAttributeNames, Map<String, Object> expressionAttributeValues) {
		initDynamoDbClient();
		return  dynamoDb.getTable(table).deleteItem(primaryKey, index, expression, expressionAttributeNames, expressionAttributeValues);
	}

	@Override
	public ItemCollection<QueryOutcome> getDataWithQuery(String table, QuerySpec query) {		
		initDynamoDbClient();
		ItemCollection<QueryOutcome> items = dynamoDb.getTable(table).query(query);
		return items;
	}

	@Override
	public ScanResult getDataWithScan(String table, ScanRequest query) {
//		initDynamoDbClient();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();		
		return client.scan(query);
	}

	@Override
	public UpdateItemOutcome updateRecordsWithItems(String table, UpdateItemSpec updateItemSpec) {
		initDynamoDbClient();
		return dynamoDb.getTable(table).updateItem(updateItemSpec);
	}

}

